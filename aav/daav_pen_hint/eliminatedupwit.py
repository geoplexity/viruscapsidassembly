
def transnode(node_num,path):
    """
    
    Arguments:
    - `node_num`:
    - `outf`:
    """
    fnode = open('Node%d.txt'%(node_num),'r')
    outf = open(path+'/Node%d.txt'%(node_num),'w')
    witset = set()
    getwit = False
    witline = ""
    dup_count = 0
    for line in fnode:
        if len(line) < 1:
            getwit = False
            continue
        elif line[0] == 'w':
            getwit = True
            witline = line
        elif line[0] == 'o':
            if getwit == True:
                getwit = False
                orr = ' '.join(line.split()[1:37])
                if orr in witset:
                    dup_count+=1
                else:
                    outf.write(witline)
                    outf.write(line)
                    witset.add(orr)
            else:
                outf.write(line)
                
        else:
            getwit = False
            outf.write(line)
    fnode.close()
    outf.close()
    print('dup ',dup_count)
        

num_node = 2494+1
#num_node = 1
for i in range(num_node):
    print('[%d]'%(i))
    transnode(i,'./res')
