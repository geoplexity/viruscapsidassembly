#!/usr/bin/python

import sys
f = open(sys.argv[1],'rb')
lin_num = 0
for line in f:
    lin_num = lin_num + 1
    row = line.split()
    
    if len(row) < 1:
        continue

    if row[0] == 'ATOM':
        print(' '.join([row[0],row[1],row[5]+row[2],row[6],row[7],row[8],'1.00']))
    elif row[0] == 'END':
        break

f.close()
