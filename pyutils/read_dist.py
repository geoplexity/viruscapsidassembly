def read_dist(filename):
    dists = []
    with open(filename,'r') as fd:
        for line in fd:
            row = line.split()
            if len(row) < 3:
                continue
            dists.append((row[0],row[1],float(row[2])))

    dists.sort(key=lambda x:x[0])
    return dists

if __name__ == '__main__':
    import sys
    distfile = sys.argv[1]
    dists = read_dist(distfile)
    for dist in dists:
        print('%s %s %f'%dist)
