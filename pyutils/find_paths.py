DIM = 0
PEN = 1
HEX = 2
D2 = 3
T1 = 4
T2 = 5
T3 = 6

interface_name = ['dim', 'pen', 'hex', 'd2', 't1', 't2', 't3']

import itertools

monomers = ['A1', 'A2','A3', 'A4', 'A5',
            'B1', 'B2', 'B3', 'B4', 'B5','B6', 'B7', 'B8', 'B9', 'B10',
            'C1', 'C2', 'C3', 'C4', 'C5','C6', 'C7', 'C8', 'C9', 'C10' 
            ]

edges = {
          
          ('B1', 'A2'): D2,
          ('B2', 'A3'): D2,
          ('B3', 'A4'): D2,
          ('B4', 'A5'): D2,
          ('B5', 'A1'): D2,

          ('A1', 'A2'): PEN,
          ('A2', 'A3'): PEN,
          ('A3', 'A4'): PEN,
          ('A4', 'A5'): PEN,
          ('A1', 'A5'): PEN,

          ('C2', 'B1'): HEX,
          ('C3', 'B2'): HEX,
          ('C4', 'B3'): HEX,
          ('C5', 'B4'): HEX,
          ('C1', 'B5'): HEX,          
          
          ('C1', 'A1'): T1,
          ('C2', 'A2'): T1,
          ('C3', 'A3'): T1,
          ('C4', 'A4'): T1,          
          ('C5', 'A5'): T1,

          ('C1', 'B1'): T3,
          ('C2', 'B2'): T3,
          ('C3', 'B3'): T3,
          ('C4', 'B4'): T3,
          ('C5', 'B5'): T3,
          ('C6', 'B6'): T3,
          ('C7', 'B7'): T3,
          ('C8', 'B8'): T3,
          ('C9', 'B9'): T3,
          ('C10', 'B10'): T3,

          
          ('A1', 'B1'): T2,
          ('A2', 'B2'): T2,
          ('A3', 'B3'): T2,
          ('A4', 'B4'): T2,
          ('A5', 'B5'): T2,

          ('C1', 'C6'): DIM,
          ('C1', 'B6'): HEX,
          ('C6', 'B1'): HEX,

          ('C2', 'C7'): DIM,
          ('C2', 'B7'): HEX,
          ('C7', 'B2'): HEX,

          ('C3', 'C8'): DIM,
          ('C3', 'B8'): HEX,
          ('C8', 'B3'): HEX,

          ('C4', 'C9'): DIM,
          ('C4', 'B9'): HEX,
          ('C9', 'B4'): HEX,

          ('C5', 'C10'): DIM,
          ('C5', 'B10'): HEX,
          ('C10', 'B5'): HEX,

          }

          
def connected(g):

    neighbour = {}
    visited = {}
    for i in monomers:
        neighbour[i] = []
        visited[i] = False

    for edge in g:
        neighbour[edge[0]].append(edge[1])
        neighbour[edge[1]].append(edge[0])

    stack = ['C1']
    # BFS
    while len(stack) > 0:
        current = stack.pop()
        if visited[current] == True:
            continue
        
        visited[current] = True
        stack.extend(filter(lambda x:visited[x] == False, neighbour[current]))

    #print(visited)
    return [x for x in visited if visited[x] == False]

g = [edge for edge in edges.keys()]

for i in range(3,7):
    for interfaces in itertools.combinations(range(7),i):
        
        graph = [edge for edge in edges.keys() if edges[edge] in interfaces]
        #print(graph)
        if len(connected(graph)) == 0:
            print( [interface_name[k] for k in list(interfaces)] )
            #print(connected(graph))
            
        
print(connected(g))
