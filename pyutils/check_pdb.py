#!/usr/bin/python

import sys
import math
import readline
import numpy as np

from read_pdb import read_pdb
from read_dist import read_dist

def dist(p1,p2):
    return np.linalg.norm(p1-p2)


#filename1 = raw_input('please enter the filename of 1st pdb:');
#filename2 = raw_input('please enter the filename of 2nd pdb:');

#filename1 = "newaav.pdb"
#filename2 = "new5f3.txt"

filename1 = sys.argv[1]
filename2 = sys.argv[2]


pdb1 = read_pdb(filename1)
pdb2 = read_pdb(filename2)


#dist_filename = raw_input('please enter the file name of distance:')
#dist_filename = "dist5.txt"
dist_filename = sys.argv[3]

dists = read_dist(dist_filename)

new_dists = []
for a1,a2,dis in dists: 
    dis1 = None
    dis2 = None
    if a1 in pdb1 and a2 in pdb2:
        dis1 = dist(pdb1[a1],pdb2[a2])
    if a1 in pdb2 and a2 in pdb1:
        dis2 = dist(pdb2[a1],pdb1[a2])

    if dis1 == None: 
        print('flip %s %s',(a1, a2))
        new_dists.append((a2,a1,dis,dis2))
    elif dis2 == None: 
        new_dists.append((a1,a2,dis,dis1))
    else:
        if abs(dis1-dis) < 0.001 or abs(dis1-dis) < abs(dis2-dis):
            new_dists.append((a1,a2,dis,dis1))
        else:
            print('flip %s %s %f %f'%(a1,a2,dis1,dis2))
            new_dists.append((a2,a1,dis,dis2))


new_dists.sort(key=lambda x:x[0])
for d in new_dists:
	print '\t'.join(map(str,d))

li = {}


while True:
    aname1 = raw_input('please enter the 1st atom:')
    aname2 = raw_input('please enter the 2nd atom:')
    print pdb1[aname1]
    print pdb2[aname2]
    print dist(pdb1[aname1],pdb2[aname2])
