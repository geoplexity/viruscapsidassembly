from matplotlib import *
from pylab import *

x = arange(0.001,3, 0.001)

sigma = 1.0

L = 4 * ( (sigma/x)**12 - (sigma/x)**6)

plt.plot(x,L)
plt.axhline(0, color='black')
plt.axvline(0, color='black')

plt.axvspan(1.0, 1.5, facecolor='b', alpha=0.2)

plt.axvline(1.0, color='black')
plt.axvline(1.5, color='black')

plt.annotate("$l$", [1.01,0.05], size="large")
plt.annotate("$u$", [1.51,0.05], size="large")

plt.xlabel(r"$r$", size = 'x-large')
plt.ylabel(r"$E_{LJ}$", size = 'x-large')

plt.xlim(left=0.5, right=2.5)
plt.ylim(-2, 5)
plt.show()
