import sys
import re

from collections import Counter

# field of ratio_stat file
ALL_COPIES = 0        # all zero-dim realizations
ALL_DISTINCT = 1      # all distinct realizations
VALID_DICTINCT = 2    # all vaild zero-dim realizations (satisfied all constraint)
TRUE_COPIES = 3       # all true zero-dim realizations (close enough to the real one)
TRUE_DISTINCT = 4     # all true distinct zero-dim realizations
NUM_COLLISION = 5
ALL_DISTINCT_WEIGHTED = 6
TRUE_DISTINCT_WEIGHTED = 7


# ids of the data of the return of read_ratio_stat
data_names = ["all", "true", "unweighted_ratio", "distinct_all", "distinct_true", "weighted_ratio"]


def extract_name_from_pair(h1, h2, pair, use_residue):
    """ pair: (id1,id2) """
    res_pattern = re.compile(r'(\d+)(.*)')
    id1,id2 = pair
    name1 = h1[id1]
    name2 = h2[id2]
    if use_residue:
        name1, name2 = tuple(map(lambda x: res_pattern.search(x).group(1), [name1, name2]))

    return name1, name2

def read_ratio_stat(dist_file, pairs_file, ratio_file, use_residue):
    total = []
    true = []
    
    distinct_total = []
    distinct_true = []

    ratio = []
    weighted_ratio = []

    h1 = []
    h2 = []

    dist_list = []
    pairs_named = []
    
    """ read dist_file """ 
    with open(dist_file,'r') as f:
        for line in f:
            segs = list(map(str,re.split(r'\s+',line.strip())))
            if len(segs) < 2:
                continue
            
            dist_list.append((segs[0],segs[1]))
            
            if not (segs[0] in h1):
                h1.append(segs[0])
                
            if not (segs[1] in h2):
                h2.append(segs[1])

    """ read pair_file """
    with open(pairs_file,'r') as f:
        name_candidates = []
        for line in f:
            if len(line.strip()) == 0:
                continue

            _pairs =  re.split(r'\s+',line.strip())
            if len(_pairs) == 1:  # only one pai
                print(_pairs)
                id1, id2 = [int(seg) for seg in _pairs[0].strip().split(',')]
                # id1 = segs[0]
                # id2 = segs[1]
                pairs_named.append( '%s-%s' % (h1[id1], h2[id2]) )
                name_candidates.append(['%s-%s' % (h1[id1], h2[id2])])
            else:  
                # multiple pairs, find the common atom/residue
                #
                # TODO:
                # way too complicated right now, need to be fixed
                # one solution is adding information when generating the pairs.txt
                
                # print(_pairs)
                freqs = Counter()

                for _pair in _pairs:
                    # print(_pair)
                    name1, name2 = extract_name_from_pair(
                        h1, h2,
                        map(int,_pair.strip().split(',')), use_residue
                    )
                    # print(name1, name2)
                    freqs[name1] += 1
                    freqs[name2] += 1

                name_found = [ name for name, freq in freqs.items() if freq >= len(_pairs) ]

                #name_candidates.append(name_found)
                                    
                if len(name_found) == 0:
                    print(freqs)
                    print("error")
                    sys.exit(-1)
                elif len(name_found) == 1:
                    # print("1")
                    # print(name_found)
                    if name_found[0] in pairs_named:
                        index = pairs_named.index(name_found[0])
                        # print(pairs_named, name_candidates[index])
                        name_candidates[index].remove(name_found[0])
                        pairs_named[index] = name_candidates[index][0]
                        
                    pairs_named.append(name_found[0])
                    name_candidates.append(name_found)
                else:
                    new_names = [name for name in name_found if name not in pairs_named]
                    pairs_named.append(new_names[0])
                    name_candidates.append(new_names)
                    
    pairs_named[0] = "None"                
    # print(pairs_named)

    """ read ratio stats file """
    with open(ratio_file,'r') as f:
        for line in f:
            segs = [float(seg) for seg in line.strip().split(',')]
            if len(segs) < 4:
                continue 
            total.append(segs[ALL_COPIES])
            true.append(segs[TRUE_COPIES])
            distinct_total.append(segs[ALL_DISTINCT])
            distinct_true.append(segs[TRUE_DISTINCT])
            if float(segs[ALL_COPIES]) > 0:
                ratio.append(float(segs[TRUE_DISTINCT])/float(segs[ALL_DISTINCT]))
                #ratio.append(float(segs[TRUE_COPIES])/float(segs[ALL_COPIES]))
                weighted_ratio.append(float(segs[TRUE_DISTINCT_WEIGHTED])/float(segs[ALL_DISTINCT_WEIGHTED]))
            else:
                ratio.append(0.0)
                weighted_ratio.append(0.0)

    # make sure pairs_file match ratio_stat file
    assert(len(ratio) == len(pairs_named))
    
    total_s = [ele/total[0] for ele in total ]
    true_s =  [ele/true[0] for ele in true] 
    ratio_s = [ele/ratio[0] for ele in ratio ]
    
    distinct_total_s = [ele/distinct_total[0] for ele in distinct_total ]
    distinct_true_s = [ele/distinct_true[0] for ele in distinct_true ]
    weighted_ratio_s = [ele/weighted_ratio[0] for ele in weighted_ratio ]

    
    # don't return None:(1,1,1)
    return {pairs_named[i]:(total_s[i], true_s[i], ratio_s[i], distinct_total_s[i], distinct_true_s[i], weighted_ratio_s[i]) \
            for i in range(1, len(ratio))}


if __name__ == '__main__':
    # test
    print(read_ratio_stat(sys.argv[1], sys.argv[2], sys.argv[3], True))
