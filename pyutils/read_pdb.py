import numpy as np

def read_pdb(filename):
    pdb = {}
    with open(filename,'rb') as f:
        for line in f:
            row = line.split()
            pdb[row[2]] = np.array(map(float,row[3:6]))
    return pdb
