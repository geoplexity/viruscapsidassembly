import os

def read_helix_config_abs(filename):
    config = {}
    with open(filename,'r') as fd:
        lines = fd.readlines()
        config['helixA'] = lines[0][1:].strip()
        config['helixB'] = lines[1][1:].strip()
        config['workspace'] = lines[2][1:].strip()
        config['dist'] = lines[11][1:].strip()

    return config

def read_helix_config(filename):
    dirname = os.path.dirname(filename)
    config = {}
    with open(filename,'r') as fd:
        lines = fd.readlines()
        config['helixA'] = os.path.join(dirname,os.path.basename(lines[0][1:].strip()))
        config['helixB'] = os.path.join(dirname, os.path.basename(lines[1][1:].strip()))
        config['workspace'] = dirname
        config['dist'] = os.path.join(dirname, os.path.basename(lines[11][1:].strip()))

    #print(config)

    return config
if __name__ == '__main__':
    import sys
    configfile = sys.argv[1]
    config = read_helix_config(configfile)
    print(config)
