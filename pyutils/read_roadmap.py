import numpy as np
import sys
import os

from collections import namedtuple

# class Node(object):
#     def __init__(self, node_number, is_complete, has_good, dimension, connections):
#         "docstring"
#         self._node_number = node_number
#         self._is_complete = is_complete
#         self._has_good = has_good
Node = namedtuple("Node", ["node_number", "is_complete", "has_good", "dimension", "connections"])

roadmap_file = sys.argv[1]

def read_roadmap(filename):

    nodes = []

    with open(roadmap_file, 'r') as f:
        f.readline()

        line = f.readline()

        while line !=  "":              # EOF
            segs = line.split()

            node_number = int(segs[1])
            is_complete = segs[2]
            has_good = segs[3]
            dimension = int(segs[4])

            num_connections = int(segs[8])
            connections = [] 
            for i in range(num_connections):
                connections.append(int(segs[i+9]))

            new_node = Node._make([node_number, is_complete, has_good, dimension, connections])
            print(node_number)

            nodes.append(new_node)

            line = f.readline()

    return nodes

def read_node_header(node_number, prefix='.'):
    filename = os.path.join(prefix, "Node{}.txt".format(node_number))
    contacts = []
    parameters = []
    with open(filename, 'r') as f:
        # skip the first 3 lines
        f.readline()
        f.readline()
        f.readline()

        for i in range(6):
            line = f.readline()
            segs = line.split()
            if segs[0] == "contact":
                contacts.append(( segs[1], segs[2]))
            elif segs[0] == "parameter":
                parameters.append(( segs[1], segs[2]))
            else:
                print("error reading node header")
                sys.exit(-1)

    return contacts, parameters

def read_node_realizations(node_number, prefix='.'):
    filename = os.path.join(prefix, "Node{}.txt".format(node_number))
    contacts = []
    parameters = []
    with open(filename, 'r') as f:
        line = f.readline()

        while line != "":
            if line[0] == 'o':
                segs = line.split()
                
            f.readline()

    return contacts, parameters

    
def count_node_realizations(node_number, prefix='.'):
    print("reading node {}".format(node_number))
    filename = os.path.join(prefix, "Node{}.txt".format(node_number))
    print(filename)
    num_samples = 0
    num_spacepts = 0

    with open(filename, 'r') as f:
        # skip the first 3 lines
        f.readline()
        f.readline()
        f.readline()
        
        line = f.readline()

        while line != "":
            if line[0] == 'o':
                num_samples += 1
            elif line[0] == '*':
                num_spacepts += 1
                
            line = f.readline()

    return num_spacepts, num_samples

nodes = read_roadmap(roadmap_file)
prefix = os.path.dirname(roadmap_file)


g_num_samples = 0
g_num_spacepts = 0

for node in nodes:
    # print(read_node_header(node.node_number, prefix))
    num_spacepts, num_samples = count_node_realizations(node.node_number, prefix)
    g_num_samples += num_samples
    g_num_spacepts += num_spacepts
    print('{}, {}'.format(g_num_samples, g_num_spacepts))

    # ignore others for now

print('{}, {}'.format(g_num_samples, g_num_spacepts))
    

