from collections import Counter
import itertools
import numpy as np

#from mayavi import mlab
eps = 0.001

def get_vertex_idx(vlist, v):
    for idx, vertex in enumerate(vlist):
        if np.linalg.norm(vertex - v) < eps:
            return idx
    return -1

def find_triangles_with_edge(flist, e):
    eset = set(e)
    return [face for face in flist if eset.issubset(set(face))]

def find_common_points(f1,f2):
    return set(f1).intersection(set(f2))
    

def find_neighbours(flist, f):
    return [face for face in flist if len(find_common_points(face, f)) == 2]
        

filename = "/home/ruijin/ico2.obj"

vertices = []
faces = []
edges = []

with open(filename, 'r') as f:
    for line in f:
        if line[0] == 'v':
            vertices.append(np.array([float(coord) for coord in line.split()[1:]]))
        elif line[0] == 'f':
            faces.append(tuple(int(idx)-1 for idx in line.split()[1:]))

# print(vertices)
# print(faces)

valence = Counter()

for face in faces:
    for idx in face:
        valence[idx] += 1
    for edge in itertools.combinations(face, 2):
        e = tuple(sorted(edge))
        if e not in edges:
            edges.append(e)

# print(edges)

nvertices = []
nfaces = []

for vertex in vertices:
    nvertices.append(vertex)

for face in faces:
    center = np.mean(np.vstack([vertices[i] for i in face]), axis=0)
    center /= np.linalg.norm(center)
    nvertices.append(center)
    # print(center)

for edge in edges:
    f1, f2 = find_triangles_with_edge(faces, edge)
    
    c1 = np.mean(np.vstack([vertices[i] for i in f1]), axis=0)
    c1 = c1/np.linalg.norm(c1)
    i1 = get_vertex_idx(nvertices,c1)
    
    c2 = np.mean(np.vstack([vertices[i] for i in f2]), axis=0)
    c2 = c2/np.linalg.norm(c2)
    i2 = get_vertex_idx(nvertices,c2)

    c3,c4 = edge
    i3 = get_vertex_idx(nvertices, vertices[c3])
    i4 = get_vertex_idx(nvertices, vertices[c4])
    

    nfaces.append((i3,i1,i2))
    nfaces.append((i4,i1,i2))
    

# fix orientation

tnface = []
for face in nfaces:
    v1,v2,v3 = [nvertices[idx] for idx in face]
    center = np.mean(np.vstack([v1,v2,v3]), axis=0)
    normal = np.cross(v2-v1, v3-v1)
    if np.dot(center, normal) < 0:
        tnface.append((face[0], face[2], face[1]))
    else:
        tnface.append(face)
        
nfaces = tnface

# gather connectivity information
vfaces = [[] for v in nvertices]
for fidx, face in enumerate(nfaces):
    vfaces[face[0]].append(fidx)
    vfaces[face[1]].append(fidx)
    vfaces[face[2]].append(fidx)
    
# create monomers
 
monomers = []
fmonomers = {}

for fidx, face in enumerate(nfaces):
    v1,v2,v3 = [nvertices[idx] for idx in face]
    center = np.mean(np.vstack([v1,v2,v3]), axis=0)
    lcenter = (2.0*v2 + v1)/3.0
    rcenter = (v3 + 2.0*v1)/3.0
    bcenter = (v2 + 2.0*v3)/3.0
    cidx = len(nvertices)
    lidx = cidx + 1
    ridx = cidx + 2
    bidx = cidx + 3

    # nvertices.append(center)
    # nvertices.append(lcenter)
    # nvertices.append(rcenter)
    # nvertices.append(bcenter)

    fmonomers[fidx] = (len(monomers), len(monomers) + 1, len(monomers) + 2)
    
    # monomers.append((face[0],lidx,cidx, ridx))
    # monomers.append((face[1],bidx,cidx, lidx))
    # monomers.append((face[2],ridx,cidx, bidx))

    monomers.append([v1,lcenter,center, rcenter])
    monomers.append([v2,bcenter,center, lcenter])
    monomers.append([v3,rcenter,center, bcenter])

# print("# monomers")

# for v in nvertices:
#     print("v {} {} {}".format(v[0],v[1],v[2]))

# for idx, monomer in enumerate(monomers):
#     print("o m{}".format(idx))
#     print("f {} {} {} {}".format(monomer[0]+1,monomer[1]+1,monomer[2]+1,monomer[3]+1))

pentamers = []
hexamers = []

# move monomers
# move all pentamer monomers
for vidx, v in enumerate(nvertices):
    # print(len(vfaces[vidx]))
    if len(vfaces[vidx]) == 5:
        for fidx in vfaces[vidx]:
            monomer = monomers[fmonomers[fidx][0]]
            for vidx2 in range(len(monomer)):
                monomer[vidx2] = monomer[vidx2] + 0.5 *v
            pentamers.append(monomer)
            
# move all hexamer monomers
for vidx, v in enumerate(nvertices):
    # print(len(vfaces[vidx]))
    if len(vfaces[vidx]) == 6:
        for fidx in vfaces[vidx]:
            if nfaces[fidx][1] == vidx:
                monomer = monomers[fmonomers[fidx][1]]
            elif nfaces[fidx][2] == vidx:
                monomer = monomers[fmonomers[fidx][2]]
            else:
                print("error", nfaces[fidx], vidx)
                
            for vidx2 in range(len(monomer)):
                monomer[vidx2] = monomer[vidx2] + 0.5 *v

            hexamers.append(monomer)

# print monomer obj
print("# monomers")
print("mtllib ico.mtl")

# for monomer in monomers:
#     for v in monomer:
#         print("v {} {} {}".format(v[0], v[1], v[2]))


# for midx, monomer in enumerate(monomers):
#     print("f {} {} {} {}".format(midx*4+1, midx *4 + 2, midx * 4 + 3, midx * 4 + 4))
# print(hexamers[0])
vcount = 0
hexamers_printlist = []
for monomer in hexamers:
    for v in monomer:
        print("v {} {} {}".format(v[0], v[1], v[2]))
    hexamers_printlist.append([vcount + 1 + i for i in range(4)])
    vcount += 4
    
pentamers_printlist = []
for monomer in pentamers:
    for v in monomer:
        print("v {} {} {}".format(v[0], v[1], v[2]))
    pentamers_printlist.append([vcount + 1 + i for i in range(4)])
    vcount += 4
    
print("usemtl greenMtl")
for face in hexamers_printlist:
    print("f {}".format(" ".join([str(vidx) for vidx in face])))

print("usemtl blueMtl")
for face in pentamers_printlist:
    print("f {}".format(" ".join([str(vidx) for vidx in face])))
