#!/usr/bin/python

import sys
import argparse
import re
from read_dist import *


parser = argparse.ArgumentParser(description='Generate pairs.txt.')
parser.add_argument('filename',type=str,
                   help='dist file')
parser.add_argument('-r', "--residue", action="store_true",
                   help='based on residue instead of atom')
parser.add_argument('-d', "--dimer", action="store_true",
                    help='if this is for dimer')
parser.add_argument('-v', "--verbose", action="store_true",
                   help='show more imformation')

args = parser.parse_args()

#print('filename = %s' % args.filename)
#print(args.residue)
#sys.exit(-1)

#filename = sys.argv[1]
filename = args.filename


#dists = read_dist(f)

h1 = []
h2 = []
dup = []
dup_atom = {}
dup_residue = {}
pairs = []
res_pairs = []
res_pattern = re.compile(r'(\d+)(.*)')    

def add_to_dup(name, container):
    if name not in container:
        container[name] = 1
    else:
        container[name] = container[name] + 1

    
# read the ndist file
with open(filename, 'r') as f:
    for line in f:
        parts = line.split()
        if len(parts) < 3:
            continue
        #print(parts)
        pair = (parts[0],parts[1])
        res_pair = tuple(map(lambda x: res_pattern.search(x).group(1), pair))

        pairs.append(pair)
        res_pairs.append(res_pair)
    
        if not (parts[0] in h1):
            h1.append(parts[0])
    
        if not (parts[1] in h2):
            h2.append(parts[1])

        add_to_dup(parts[0], dup_atom)
        if parts[1] != parts[0]:
            add_to_dup(parts[1], dup_atom)
	
        add_to_dup(res_pair[0], dup_residue)
        if res_pair[1] != res_pair[0]:
            add_to_dup(res_pair[1], dup_residue)

#print(dup_atom)
#print(dup_residue)

#print(dup)
done = []
print('1,0')

for pair in pairs:
    if not pair in done:
        #print('%d,%d %d,%d'%(h1.index(pair[0]),h2.index(pair[1]),h1.index(pair[1]),h2.index(pair[0])))
        #print('%s-%s'%(pair[0],pair[1]))
        print('%d,%d '%(h1.index(pair[0]),h2.index(pair[1])))
        #done.append(pair)
        #done.append((pair[1],pair[0]))


## ''' only work for dmvmp_all '''
## for atom in dup:
##     out_str = []
##     for pair in pairs:
##         if atom in pair:
##             out_str.append('%d,%d'%(h1.index(pair[0]),h2.index(pair[1])))
##     print(' '.join(out_str))
if not args.residue:
    for atom,freq in dup_atom.items():
        if freq > 2 or ( not args.dimer and freq > 1):
            out_str = []
            #print(atom)
            for pair in [p for p in pairs if atom in p]: 
                if args.verbose:
                    print(pair)
                out_str.append('%d,%d'%(h1.index(pair[0]),h2.index(pair[1])))
            print(' '.join(out_str))
else:
    for residue,freq in dup_residue.items():
        #print(residue, freq)
        if freq > 2 or ( not args.dimer and freq > 1):
            out_str = []
            
            #print(residue)
            for res_pair in [p for p in res_pairs if residue in p]:
                pidx = res_pairs.index(res_pair)
                pair = pairs[pidx]
                if args.verbose:
                    print(pair)
                out_str.append('%d,%d'%(h1.index(pair[0]),h2.index(pair[1])))
            print(' '.join(out_str))
