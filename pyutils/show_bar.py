#!/usr/bin/env python
# a bar plot with errorbars
import numpy as np
import matplotlib.pyplot as plt
import sys
import re

import argparse # only in python 2.7 and up

from read_helix_config import read_helix_config
from read_ratio_stat import read_ratio_stat

""" handle arguments """
parser = argparse.ArgumentParser()
parser.add_argument("ratio_file", help="file contains the ratio stats")
parser.add_argument("-s","--sort",help="the item used to sort")
parser.add_argument("-p","--pairs",help="the pairs file")
parser.add_argument("-d","--dist",help="the ndist file")
parser.add_argument("-c","--config",help="the helix config file")
parser.add_argument("-o","--output",help="the ouput file")
parser.add_argument("-r","--residue",action='store_true', 
                    help="based on residue instead of atom")

args = parser.parse_args()

filename  = args.ratio_file
sort_item = args.sort
output_file = args.output
use_residue = args.residue

if args.config is None:
    pairs_file = args.pairs
    dist_file = args.dist
else:
    config = read_helix_config(args.config)
    workspace = config["workspace"]
    pairs_file = workspace + '/pairs.txt'
    dist_file = config["dist"]

if sort_item:
    print(sort_item)

print("reading dist: {}".format(dist_file))
print("reading pairs: {}".format(pairs_file))

ratio_stat = read_ratio_stat(dist_file, pairs_file, filename, use_residue)


ratio_stat_list = [(k,v[0],v[1],v[2]) for k,v in ratio_stat.items()]

# sort
if sort_item:
    sort_idx = 0
    if sort_item == "total":
        sort_idx = 1
    elif sort_item == "true":
        sort_idx = 2
    elif sort_item == "ratio":
        sort_idx = 3
    else:
        print("can't sort on %s"%sort_item)
        sys.exit(-1)

    ratio_stat_list = sorted(ratio_stat_list, key = lambda x:x[sort_idx])

ratio_stat_list.insert(0,("None",1.0,1.0,1.0))

N = len(ratio_stat_list)
indice = range(N)
ind = np.arange(N)  # the x locations for the groups
width = 0.25       # the width of the bars

""" create figure """
fig = plt.figure()
ax = fig.add_subplot(111)
#rects1 = ax.bar(ind, menMeans, width, color='r', yerr=menStd)
rects1 = ax.barh(ind, [e[1] for e in ratio_stat_list], width, color='r') # total
rects2 = ax.barh(ind+width, [e[2] for e in ratio_stat_list], width, color='g') # true
rects3 = ax.barh(ind+2*width, [e[3] for e in ratio_stat_list], width, color='b') # ratio

# add some
ax.set_position([0.2,0.1,0.7,0.8])
ax.set_xlabel('Values')
ax.set_title(filename)
ax.set_yticks(ind+width)
#ax.set_xticklabels( ('G1', 'G2', 'G3', 'G4', 'G5') )
# if len(pairs_named) > 0:
ax.set_yticklabels( [e[0] for e in ratio_stat_list] )
# else:
#     ax.set_yticklabels( indice )
ax.xaxis.grid(True)
#ax.set_figwidth(1.5)
ax.set_xbound(upper = 2.0)
ax.legend( (rects1[0], rects2[0],rects3[0]), ('total', 'true','ratio'), loc='lower left', bbox_to_anchor = (0.9, 0.5) )
#ax.legend()

""" generate output file """
if output_file:
    output_done = False
    if len(output_file.split('.')) >= 2:
        file_type = output_file.split('.')[-1]
        if file_type == 'pdf' or file_type == 'eps' or file_type == 'png':
            plt.savefig(output_file,format=file_type)
            output_done = True

    if not output_done:
        print("Warning: Can't detect output file type, use PNG")
        plt.savefig(output_file+".png",format='png')
else:
    plt.show()
#plt.savefig('tt.pdf',format='pdf')

