#!/usr/bin/python

import sys
import math
import readline
import argparse
import numpy as np

import scipy
import scipy.spatial


from read_pdb import read_pdb
from read_dist import read_dist

# parser = argparse.ArgumentParser(description='Generate ndist.txt.')
# parser.add_argument('filename',type=str,
#                    help='dist file')
# parser.add_argument('-c', "--configfile", help='helix packing config file')

# args = parser.parse_args()

def dist(p1,p2):
    return np.linalg.norm(p1-p2)


#filename1 = raw_input('please enter the filename of 1st pdb:');
#filename2 = raw_input('please enter the filename of 2nd pdb:');

#filename1 = "newaav.pdb"
#filename2 = "new5f3.txt"


def gen_ndist(filename1, filename2, dist_filename):

    pdb1 = read_pdb(filename1)
    pdb2 = read_pdb(filename2)
    
    dists = read_dist(dist_filename)
    
    new_dists = []
    flip_count =  0
    for a1,a2,dis in dists: 
        dis1 = None
        dis2 = None
        if a1 in pdb1 and a2 in pdb2:
            dis1 = dist(pdb1[a1],pdb2[a2])
        if a1 in pdb2 and a2 in pdb1:
            dis2 = dist(pdb2[a1],pdb1[a2])
    
        if dis1 == None: 
            print('flip %s %s',(a1, a2))
            new_dists.append((a2,a1,dis,dis2))
        elif dis2 == None: 
            new_dists.append((a1,a2,dis,dis1))
        else:
            if abs(dis1-dis) < 0.001 or abs(dis1-dis) < abs(dis2-dis):
                new_dists.append((a1,a2,dis,dis1))
            else:
                print('flip %s %s %f %f'%(a1,a2,dis1,dis2))
                flip_count += 1
                new_dists.append((a2,a1,dis,dis2))
    
    
    new_dists.sort(key=lambda x:x[0])
    lines = ['\t'.join(map(str,d)) for d in new_dists]
    return '\n'.join(lines)

def transform_pdb_fromto(fromto, pdb):
    """ transform the atoms in a pdb using the fromto file """

    from trans import getTransMatrix, applyTransMatrix
    
    with open(fromto, 'r') as f:
        fp = []
        tp = []
        for i in range(3):
            fp.append([float(v) for v in f.readline().split()])
            tp.append([float(v) for v in f.readline().split()])

        mat = getTransMatrix(fp,tp)
        print("[mat]", mat)

    for name in pdb:
        pdb[name] = applyTransMatrix(mat, pdb[name])
        

def gen_ndist_extra(filename1, filename2, dist_filename, fromto=None):
    
    pdb1 = read_pdb(filename1)
    pdb2 = read_pdb(filename2)
    
    dists = read_dist(dist_filename)

    if fromto is not None:
        transform_pdb_fromto(fromto, pdb2)

    raw_input("press any key to continue")

    accept_distance = [2.4, 3.6]

    # build kd tree
    pdb1_list = list(pdb1.items())

    candiate_new_dist = []
    extra_atoms = set([])

    kdtree1 = scipy.spatial.KDTree(np.vstack([p for v,p in pdb1_list]))

    for a2, p2 in pdb2.items():
        res = kdtree1.query_ball_point(p2, accept_distance[1])
        # if a2 == '337OG1':
        #     for idx in res:
        #         a1, p1 = pdb1_list[idx]
        #         print(a1, dist(p1,p2))
            
        for idx in res:
            a1, p1 = pdb1_list[idx]
            if dist(p1,p2) > accept_distance[0]:
                candiate_new_dist.append((a1,a2, dist(p1,p2)))
                extra_atoms.add((a1,1))
                extra_atoms.add((a2,2))

    print(candiate_new_dist)

    # pick the new pair that is far away from the existing pairs

    extra_atom_min_dist = {}
    for atom, pdb_idx in extra_atoms:
        if pdb_idx == 1:
            pos = pdb1[atom]
        else:
            pos = pdb2[atom]
            
        min_dist = 100
        for a1,a2,dis in dists:
            p1 = pdb1[a1]
            p2 = pdb2[a2]
            local_min_dist = min(dist(pos, p1), dist(pos, p2))
            min_dist = min(local_min_dist, min_dist)
        if atom not in extra_atom_min_dist:
            extra_atom_min_dist[atom] = min_dist
        else:
            extra_atom_min_dist[atom] = min( extra_atom_min_dist[atom], min_dist)
            

    candiate_new_dist_sorted = sorted(candiate_new_dist, \
                                      key = lambda x: min(extra_atom_min_dist[x[0]],\
                                                          extra_atom_min_dist[x[1]]), reverse=True
                                      )
    #print(candiate_new_dist_sorted)

    # Show the first 10 extra pairs, get user input to pick the extra pairs
    residue_id = raw_input("Please select residue you want to filter, 0 mean no specific residue:")

    if residue_id == '0':
        candidate_dist_filtered = candiate_new_dist_sorted
    else:
        candidate_dist_filtered = [ (a1,a2,distance) for a1,a2,distance in candiate_new_dist_sorted \
                                    if a1.startswith(residue_id) or a2.startswith(residue_id) ]
        

    for i in range(min(40, len(candidate_dist_filtered))):
        a1, a2, distance = candidate_dist_filtered[i]
        print("[{}]: {} - {} : {}| {}".format(i, a1,a2,distance, min(extra_atom_min_dist[a1],\
                                                                     extra_atom_min_dist[a2])))

    #print("Please select the pairs you want to add, seperated by comma:",)


    pair_ids = raw_input("Please select the pairs you want to add, seperated by comma:")

    ids = [int(seg) for seg in pair_ids.split(',')]

    for pid in ids:
        a1, a2, distance = candidate_dist_filtered[pid]
        dists.append(candidate_dist_filtered[pid])
    
    
    new_dists = []
    flip_count =  0
    for a1,a2,dis in dists: 
        dis1 = None
        dis2 = None
        if a1 in pdb1 and a2 in pdb2:
            dis1 = dist(pdb1[a1],pdb2[a2])
        if a1 in pdb2 and a2 in pdb1:
            dis2 = dist(pdb2[a1],pdb1[a2])
    
        if dis1 == None: 
            print('flip %s %s',(a1, a2))
            new_dists.append((a2,a1,dis,dis2))
        elif dis2 == None: 
            new_dists.append((a1,a2,dis,dis1))
        else:
            if abs(dis1-dis) < 0.001 or abs(dis1-dis) < abs(dis2-dis):
                new_dists.append((a1,a2,dis,dis1))
            else:
                print('flip %s %s %f %f'%(a1,a2,dis1,dis2))
                flip_count += 1
                new_dists.append((a2,a1,dis,dis2))
    
    
    new_dists.sort(key=lambda x:x[0])
    lines = ['\t'.join(map(str,d)) for d in new_dists]
    return '\n'.join(lines)


if __name__ == '__main__':    
    filename1 = sys.argv[1]
    filename2 = sys.argv[2]
    dist_filename = sys.argv[3]

    fromto_filename = None
    if len(sys.argv) > 4:       # use fromto
        fromto_filename = sys.argv[4]

    print(gen_ndist_extra(filename1, filename2, dist_filename, fromto=fromto_filename))


#while True:
#    aname1 = raw_input('please enter the 1st atom:')
#    aname2 = raw_input('please enter the 2nd atom:')
#    print pdb1[aname1]
#    print pdb2[aname2]
#    print dist(pdb1[aname1],pdb2[aname2])
