import os

def check_node_good(node_id, prefix):
    with open(os.path.join(prefix, "Node{}.txt".format(node_id)), 'r') as f:
        for line in f:
            if line[0] == 'b':
                return True
                
    return False

def read_roadmap_old(prefix):
    nodes = []
    
    with open(os.path.join(prefix, "RoadMap.txt"), 'r') as f:
        for line in f:
            if line[0] == '/':
                continue
            elif line[0] == '#':
                nodes.append(line.strip().split())
    return nodes

def convert_roadmap_new(prefix, nodes):
    for node in nodes:
        node_id = node[1]
        node_good = check_node_good(node_id, prefix)
        
        node.insert(2, ('1' if node_good else '0'))
        print(" ".join(node))

if __name__ == '__main__':
    nodes = read_roadmap_old(".")
    convert_roadmap_new(".", nodes)
