import logging
import sys
import os
import numpy as np

from mayavi import mlab

from read_pdb import read_pdb
from read_dist import read_dist
from read_helix_config import read_helix_config

helix_config = read_helix_config(sys.argv[1])

h1 = read_pdb(helix_config["helixA"])
h2 = read_pdb(helix_config["helixB"])
dist = read_dist(helix_config["dist"])

def add_pdb(helix, color = (1,1,1)):
    m = np.vstack(helix.values())
    s = mlab.points3d(m[:,0],m[:,1],m[:,2],
                      scale_factor = 2.0, transparent = False, opacity=1.0,
                      color=color)
    return s

def add_dist(ha, hb, vecs, color = (1.0,0.0,0.0)):
    l1 = []
    l2 =[]
    for a1,a2,d in vecs:
        l1.append(ha[a1])
        l2.append(hb[a2])
    m1 = np.vstack(l1)
    m2 = np.vstack(l2)

    s = mlab.points3d(m1[:,0],m1[:,1],m1[:,2],
                      scale_factor = 2.2, transparent = False, opacity = 1.0,
                      color=(0.5,0,0.7))
    s = mlab.points3d(m2[:,0],m2[:,1],m2[:,2],
                      scale_factor = 2.2, transparent = False, opacity = 1.0,
                      color=(0.5,0.7,0))

    m2 = m2 - m1
    print(m1,m2)

    return mlab.quiver3d(m1[:,0],m1[:,1],m1[:,2],m2[:,0],m2[:,1],m2[:,2], scale_factor = 1.0, color=color)

blue = (0.0, 0.0, 1.0)
green = (0.0, 1.0, 0.0)


# print(h1)
# print(h2)

mlab.figure(bgcolor=(1,1,1))
add_pdb(h1, blue)
add_pdb(h2, green)
#add_dist(h1,h2,dist)
mlab.show()

# View it.
#s = mlab.mesh(x, y, z)
