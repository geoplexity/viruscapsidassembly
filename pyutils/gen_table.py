#!/usr/bin/env python
# read all the ratio_stat for a interface generate the ranking table
#  for Latex

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook

import random
import os
import sys
import re
import json
import itertools

import argparse # only in python 2.7 and up

from read_helix_config import read_helix_config
from read_ratio_stat import read_ratio_stat
from read_ratio_stat import data_names
from unify_bmv import convert_ratio_stat_residue

""" handle arguments """
parser = argparse.ArgumentParser()

parser.add_argument("folder", help="folder")
parser.add_argument("interface", help="file contains the ratio stats")
parser.add_argument("-r","--ratio_stat_file",help="actually ratio_stat file name")
parser.add_argument("-l","--lloc",help="legend location")
parser.add_argument("-n","--no_label",action='store_true', 
                    help="do not show labels")
parser.add_argument("-o","--output",help="output file")
parser.add_argument("-xy","--xy_axis", help="sepecify the axis")
#parser.add_argument("result_file",help="the result file")

#parser.add_argument("ratio_file", help="file contains the ratio stats")

args = parser.parse_args()

# filename  = args.ratio_file

if args.ratio_stat_file is None:
    # use the default file name
    filename = os.path.join(args.folder,"ratio_stat", "ratio_stat_"+ args.interface +".csv")
else:
    # use the user specified file name
    filename = os.path.join(args.folder,"ratio_stat", "ratio_stat_"+ args.ratio_stat_file +".csv")
    

    
print(filename)
output_file = args.output
use_residue = True
result_file = os.path.join(args.folder, "RESULT.json")

if args.lloc is not None:
    legend_location = int(args.lloc)
else:
    legend_location = 0

# if args.config is None:
#     pairs_file = args.pairs
#     dist_file = args.dist
# else:
config = read_helix_config(os.path.join(args.folder, args.interface+"_hint", "HelixPacking.cfg"))
print(config)
workspace = config["workspace"]
pairs_file = workspace + '/pairs.txt'
dist_file = config["dist"]


print("reading dist: {}".format(dist_file))
print("reading pairs: {}".format(pairs_file))

ratio_stat = read_ratio_stat(dist_file, pairs_file, filename, use_residue)
if use_residue:
    ratio_stat = convert_ratio_stat_residue(ratio_stat)

# reset ranking score
ranking_score = {k:0 for k in  ratio_stat}
print(ranking_score)

# read the raw data
ratio_stat_list_unsorted = list(ratio_stat.items())
print(ratio_stat_list_unsorted)

# sort the residues according to the true_copies and true_ratio
sort_index_names = ["distinct_all", "weighted_ratio"]
#sort_index_names = ["distinct_all", "weighted_ratio"]
#sort_index_names = ["distinct_true", "weighted_ratio"]
#sort_index_names = ["distinct_true", "unweighted_ratio"]

if args.xy_axis is not None:
    sort_index_names = args.xy_axis.split(',')
    
try:
    sort_indices = [data_names.index(name) for name in sort_index_names]
except Exception:
    print("illegal axises: {}, {}, use default axis.".format(sort_index_names[0], sort_index_names[1]))
    sort_index_names = ["distinct_all", "weighted_ratio"]
    sort_indices = [data_names.index(name) for name in sort_index_names]
        
print("Sort and plot using data:{}, {}".format(sort_index_names[0], sort_index_names[1]))
print(sort_indices)

for sort_idx in sort_indices:
    ratio_stat_list = [k for k,v in sorted(ratio_stat_list_unsorted, key = lambda x:x[1][sort_idx])]
    print("sorting by {}".format(data_names[sort_idx]))
    for k in ratio_stat.keys():
        ranking_score[k] += ratio_stat_list.index(k)
        print(k, ratio_stat_list.index(k))
    print()
    
ranking_score_sorted = sorted(list(ranking_score.items()), key=lambda x: x[1])
    
for k,s in ranking_score_sorted:
    print(k, s)


# # Marker size in units of points^2
# volume = (15 * price_data.volume[:-2] / price_data.volume[0])**2
# close = 0.003 * price_data.close[:-2] / 0.003 * price_data.open[:-2]
lab_ranking = None
if result_file:
    with open(result_file, 'r') as f:
        result = json.load(f)
    lab_ranking = result["ranking_result"]
    denial_lab_rank = max(lab_ranking.values())

# incase there no denail result
if denial_lab_rank == 0:
    denial_lab_rank = 1
    
# use different marker for residues according to their lab result
NA_list = []
NO_list = []
YES_list = []

for k,v in ratio_stat_list_unsorted:
    if k not in lab_ranking:
        NA_list.append(k)
    elif lab_ranking[k] == denial_lab_rank:
        NO_list.append(k)
    else:
        YES_list.append(k)

fig, ax = plt.subplots()
#ax.scatter([v[2] for v in ratio_stat_list_unsorted],[v[3] for v in ratio_stat_list_unsorted], \
#           marker = markers)

ax.scatter([ratio_stat[residue][sort_indices[0]] for residue in NA_list],\
           [ratio_stat[residue][sort_indices[1]] for residue in NA_list], \
           marker = '.')
ax.scatter([ratio_stat[residue][sort_indices[0]] for residue in YES_list],\
           [ratio_stat[residue][sort_indices[1]] for residue in YES_list], \
           marker = 'x')
ax.scatter([ratio_stat[residue][sort_indices[0]] for residue in NO_list],\
           [ratio_stat[residue][sort_indices[1]] for residue in NO_list], \
           marker = 's')

ax.legend(["N/A", "Crucial", "Non-crucial"], loc=legend_location)

# add label to the data points

# y offset between labels and the data points

x_range = (ax.axis()[1] - ax.axis()[0])
y_range = (ax.axis()[3] - ax.axis()[2])

y_offset = y_range / 40.0

annotate_data_pos = {}
annotate_pos = {}
annotate_vec = {}

time_step = 0.01
annotate_mass = 20

if not args.no_label:
    for k, v in ratio_stat_list_unsorted:
        # Put the text near the data point.
        # Add some randomness so that labels will not overlap
        # too much when the data pointsa re close.
        #
        # TODO: advanced layout algorithm couls be used here to avoid overlap
        # print(v, sort_indices[0])
        
        # print(type(v[sort_indices[0]]))
        # print(type(v[sort_indices[1]]))
        annotate_data_pos[k] = v[sort_indices[0]],v[sort_indices[1]]
        # ax.annotate("{}".format(k), (v[sort_indices[0]],v[sort_indices[1]]), fontsize=10, \
        #             arrowprops=dict(arrowstyle="->"), \
        #             xytext = (v[sort_indices[0]] +            random.random() * x_range * 0.1 - x_range * 0.05,
        #                       v[sort_indices[1]] + y_offset + random.random() * y_range * 0.03 ))

# iterative method of arrage annotate position

for k in annotate_data_pos:
    annotate_pos[k] = annotate_data_pos[k] + np.array([random.random() * x_range * 0.04 - x_range * 0.02, \
                                                    y_offset + random.random() * y_range * 0.03 ])
    annotate_vec[k] = 0

for i in range(200):
    # update pos
    for k in annotate_pos:
        annotate_pos[k] += annotate_vec[k] * time_step
        # damping
        annotate_vec[k] -= annotate_vec[k] * np.linalg.norm(annotate_vec[k]) * 0.1 / annotate_mass * time_step
        
    # reple force
    for a,b in itertools.combinations(annotate_pos.keys(), 2):
        dis_ab = annotate_pos[b]  - annotate_pos[a]
        force = dis_ab / (np.linalg.norm(dis_ab) / x_range )**3 * 0.0002 * x_range
        annotate_vec[a] -= force / annotate_mass * time_step
        annotate_vec[b] += force / annotate_mass * time_step

    for k in annotate_pos:
        # attrac force
        dis_k = annotate_pos[k] - annotate_data_pos[k]
        force = dis_k / np.linalg.norm(dis_k) * (np.linalg.norm(dis_k) - x_range * 0.03) * 2
        annotate_vec[k] -= force / annotate_mass * time_step

        # gravity
        annotate_vec[k] += np.array([0,0.1]) / annotate_mass * time_step

for k in  annotate_pos:
    annotate_pos[k] = (annotate_pos[k] - annotate_data_pos[k]) * (1, y_range/x_range/2) + annotate_data_pos[k]
    
print(annotate_pos)
print(annotate_data_pos)
    # raw_input("press")

for k in annotate_pos:
    ax.annotate("{}".format(k), (annotate_data_pos[k]), fontsize=10, \
                arrowprops=dict(arrowstyle="->"), \
                xytext = annotate_pos[k])
        
ax.set_xlabel(sort_index_names[0], fontsize=15)
ax.set_ylabel(sort_index_names[1], fontsize=15)
# ax.set_title('Residue crucialness for {}'.format(args.interface))

ax.grid(True)
fig.tight_layout()

""" generate output file """
if output_file:
    output_done = False
    if len(output_file.split('.')) >= 2:
        file_type = output_file.split('.')[-1]
        if file_type == 'pdf' or file_type == 'eps' or file_type == 'png':
            plt.savefig(output_file,format=file_type)
            output_done = True

    if not output_done:
        print("Warning: Can't detect output file type, use PNG")
        plt.savefig(output_file+".png",format='png')
else:
    plt.show()
