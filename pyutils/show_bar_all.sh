
echo "pwd = $(pwd)"
echo "folder = $1"

for i in `find $1 -maxdepth 1 -type d`	
do
    base_part=`basename $i`
    dir_part=`dirname $i`

    #echo $i

    if [[ $base_part =~ ([^_]+)_([^_]+)_hint ]] ;
    then 
        echo $base_part $dir_part
        testcase=${BASH_REMATCH[1]}; 
        nei=${BASH_REMATCH[2]};
        filename="ratio_stat_$testcase"_"$nei".csv 
        pdffilename="ratio_stat_$testcase"_"$nei".pdf

        #python show_bar.py -c $i/HelixPacking.cfg /ratio_stat/$filename -s ratio

        if [ -a $dir_part/ratio_stat/$filename ];
        then

        echo $i/HelixPacking.cfg $dir_part/ratio_stat/$filename -s ratio -o $pdffilename

        python show_bar.py -c $i/HelixPacking.cfg $dir_part/ratio_stat/$filename -s ratio -o $pdffilename -r > /dev/null

        fi
    else
        echo $base_part " not match"
    fi
done
