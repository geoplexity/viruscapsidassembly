import pickle
import os
import json
import argparse # only in python 2.7 and up

""" handle arguments """
parser = argparse.ArgumentParser()
parser.add_argument("folder", help="folder")
parser.add_argument("learning_result", help="file contains the ratio stats")

args = parser.parse_args()

result_file = os.path.join(args.folder, "RESULT.json")

# read the lab result
if result_file:
    with open(result_file, 'r') as f:
        result = json.load(f)
    lab_ranking = result["ranking_result"]
    denial_lab_rank = max(lab_ranking.values())
    
# incase there no denail result
if denial_lab_rank == 0:
    denial_lab_rank = 1

print(lab_ranking)
    
with open(args.learning_result, 'rb') as f:
   learning_result = pickle.load( f )
   for k,v in learning_result:
       print("{} & {}\\\\".format(k, lab_ranking[k] if k in lab_ranking else " "))

# draw a color coded table using matplotlib
import numpy as np
import matplotlib.pyplot as plt

#clust_data = [[k, ("Yes" if lab_ranking[k]!=denial_lab_rank else "No") if k in lab_ranking else " "] for k,v in learning_result]
clust_data = [[k, ""] for k,v in learning_result]
colors = [[(1,1,1,1), plt.cm.RdYlBu(1-lab_ranking[k]/float(denial_lab_rank)) if k in lab_ranking else (1,1,1,1)] for k,v in learning_result]
print(clust_data)

colLabels=("Residue", "Is Crucial")
nrows, ncols = len(clust_data)+1, len(colLabels)
hcell, wcell = 0.3, 1.
hpad, wpad = 0.0, 0.0    
fig=plt.figure(figsize=(ncols*wcell+wpad, nrows*hcell+hpad))
ax = fig.add_subplot(111)
ax.axis('off')
print(ax.bbox)
#do the table
the_table = ax.table(cellText=clust_data,
                     cellColours=colors,
                     colLabels=colLabels,
                     bbox = (0,0,ncols*wcell*0.5,nrows*hcell*0.1),
          loc='center')


# data = [[  66386,  174296,   75131,  577908,   32015],
#         [  58230,  381139,   78045,   99308,  160454],
#         [  89135,   80552,  152558,  497981,  603535],
#         [  78415,   81858,  150656,  193263,   69638],
#         [ 139361,  331509,  343164,  781380,   52269]]

# columns = ('Freeze', 'Wind', 'Flood', 'Quake', 'Hail')
# rows = ['%d year' % x for x in (100, 50, 20, 10, 5)]

# values = np.arange(0, 2500, 500)
# value_increment = 1000

# # Get some pastel shades for the colors
# colors = plt.cm.BuPu(np.linspace(0, 0.5, len(columns)))
# n_rows = len(data)

# index = np.arange(len(columns)) + 0.3
# bar_width = 0.4

# # Initialize the vertical-offset for the stacked bar chart.
# y_offset = np.array([0.0] * len(columns))

# #ax = subplot(111, frame_on=False)
# fig = plt.figure()
# fig.patch.set_alpha(0.5)
# ax = fig.add_subplot(111)
# ax.patch.set_alpha(0.5)

# ax.xaxis.set_visible(False) 
# ax.yaxis.set_visible(False)

# # Plot bars and create text labels for the table
# cell_text = []
# for row in range(n_rows):
#     # plt.bar(index, data[row], bar_width, bottom=y_offset, color=colors[row])
#     y_offset = y_offset + data[row]
#     cell_text.append(['%1.1f' % (x/1000.0) for x in y_offset])
# # Reverse colors and text labels to display the last value at the top.
# colors = colors[::-1]
# cell_text.reverse()

# # Add a table at the bottom of the axes
# the_table = plt.table(cellText=cell_text,
#                       rowLabels=rows,
#                       rowColours=colors,
#                       colLabels=columns,
#                       loc='center'
# )
# #                      loc='bottom')

# # Adjust layout to make room for the table:
# plt.subplots_adjust(left=0.2, bottom=0.1)

# # plt.ylabel("Loss in ${0}'s".format(value_increment))
# # plt.yticks(values * value_increment, ['%d' % val for val in values])
# # plt.xticks([])
# # plt.title('Loss by Disaster')


plt.show()
