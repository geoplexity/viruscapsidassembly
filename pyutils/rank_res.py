from __future__ import division
import json
import sys
from collections import Counter
import itertools
import random
import math
import logging
# import numpypy
import numpy as np
import pickle
import argparse # only in python 2.7 and up

from unify_bmv import get_ratio_stats
import read_ratio_stat

class PreRankingModel(object):
    """
    w_r^i = alpha if r is very important in interface i ( r in crucial_res[i][0])  
    w_r^i = beta if r is important in interface i ( r in crucial_res[i][1])
    w_r^i = 0 if r is not important in interface i

    alpha and beta are the two parameters of the model
    """

    def __init__(self, result):
        self._result = result
        # self._dim = 2 + len(result["paths"])
        self._dim = 2
        self._residue_set = set()

        for interface, residues in result["crucial_res"].items():
            all_res = residues[0] + residues[1]
            for residue in all_res:
                self._residue_set.add(residue)

    def point_dim(self):
        return self._dim
        
    def residue_crucialness_in_interface(self, residue, interface, path, pars):
        alpha, beta = pars
        
        if residue in self._result["crucial_res"][interface][0]:
            return alpha
        elif residue in self._result["crucial_res"][interface][1]:
            return beta
        else:
            return 0

    def is_feasible_point(self, p):
        for val in p:
            if val < 0.0 or val > 1.0:
                return False

        if p[0] < p[1]:
            return False

        return True
    
    def projected_point(self, p):
        res = np.clip(p, 0.0, 1.0)

        if res[0] < res[1]:
            tmp = (res[0] + res[1]) / 2.0
            res[0] = tmp
            res[1] = tmp

        return res

    def random_point(self, center, radius):
        p = np.array([-1] * self._dim)

        while not self.is_feasible_point(p):
            p = (np.random.rand(self._dim) - 0.5) * radius + center
            if p[0] < p[1]:
                p[0], p[1] = p[1], p[0]

        return p
        
    def print_point(self, p):
        print("alpha: {}, beta: {}".format(p[0], p[1]))

class LinearModel(object):
    """
    determine w_r^i by a linear model of ratio and tru_copy

    the 3 * num_interface coefficients of the linear funtion are the model parameters
    """

    def __init__(self, result, ratio_stat, paths):
        self._result = result
        # self._dim = 3 + len(result["paths"])
        self._num_interfaces = len(result["interfaces"])
        self._dim = 3 * len(result["interfaces"]);
        self._ratio_stat = ratio_stat
        self._residue_set = set()

        for interface in ratio_stat.keys():
            for residue in ratio_stat[interface][0][1].keys():
                self._residue_set.add(residue)

        self._cached_stat = self._precompute_residue_stat_in_path(paths)
        self._interface_idx = {interface:idx for idx, interface in enumerate(self._result["interfaces"])}
                
    def _precompute_residue_stat_in_path(self, paths):
        """ precompute the average true and ratio over
        different neighbor for different (path, interface, residue)
        tuple.
        """

        # chose the criterions
        ratio_state_names = ["distinct_all", "weighted_ratio"]
        stat_idx_1 = read_ratio_stat.data_names.index(ratio_state_names[0])
        stat_idx_2 = read_ratio_stat.data_names.index(ratio_state_names[1])

        # stat_idx_1 = 1 
        # stat_idx_2 = 2
        
        logging.debug("indixes:{},{}".format(stat_idx_1, stat_idx_2))
        
        
        res = {}
        for path in paths:
            # make sure the path is hashable
            tpath = tuple(path)
            
            for interface in self._ratio_stat.keys():
                residue_ratio_counter = Counter()
                residue_true_counter = Counter()
                residue_nei_counter = Counter()
                
                for residue in self._residue_set:
                
                    # check to see if in the interface.
                    # since all neighbour have the same residue list,
                    # just need to check the first neighbour
                    if residue not in self._ratio_stat[interface][0][1]:
                        continue
                    
                
                    # average the stat based on the path
                    for nei, stat in self._ratio_stat[interface]:
                        # only count the neighbors that in the path
                        if nei in path or nei == 'init':
                            residue_true_counter[residue] += \
                                            stat[residue][stat_idx_1]
                            residue_ratio_counter[residue] += \
                                            stat[residue][stat_idx_2]
                            residue_nei_counter[residue] += 1
                            
                for residue in residue_true_counter.keys():
                    nei_count = residue_nei_counter[residue]
                    # print("nn {}".format(nei_count))
                    res[(tpath, interface, residue)] = \
                            ( residue_true_counter[residue]/nei_count,
                              residue_ratio_counter[residue]/nei_count )

        # logging.debug("precompute done: {}".format(res))
        return res
        
    def point_dim(self):
        return self._dim
        
    def residue_crucialness_in_interface(self, residue, interface, path, pars):
        """ Calculate the crucialness based on a linear model

        FIXME: coefficient for different interface should be different?
        """

        interface_idx = self._interface_idx[interface]
        
        a,b,c = pars[interface_idx*3:interface_idx*3 + 3]
        
        a = a * 40.0 - 20.0
        b = b * 40.0 - 20.0
        c = c * 40.0 - 20.0

        iname = self._result["interface_map"][interface]

        # check to see if in the interface.
        # since all neighbour have the same residue list,
        # just need to check the first neighbour
        if iname not in self._ratio_stat or residue not in self._ratio_stat[iname][0][1]:
            return 0

        
        true_value, ratio_value = self._cached_stat[(tuple(path), iname, residue)]
        
        p = (0 * true_value + \
            b * ratio_value + \
             c )/ 1.0

        p = sigmoid(p/20.0)

        # logging.debug("residue = {}, interface = {}, p = {}".format(residue, interface, p))

        # the crucialness will be used as probability, so it should be clipped
        return np.clip(p,0.0,1.0)
    
    def is_feasible_point(self, p):
        for val in p:
            if val < 0.0 or val > 1.0:
                return False

        return True
    
    def projected_point(self, p):
        return  np.clip(p, 0.0, 1.0)

    def random_point(self, center, radius):
        p = np.array([-1] * self._dim)

        while not self.is_feasible_point(p):
            p = (np.random.rand(self._dim) - 0.5) * radius + center

        return p

    def print_point(self, p):
        for idx, interface in enumerate(self._result["interfaces"]):
            print("{} - a: {}, b: {}, c: {}".format(interface, p[idx * 3], p[idx * 3 + 1], p[idx * 3 + 2]))

class PathCrucialModel(object):
    """ Determine the 
    """

    def __init__(self, model):
        "docstring"
        self._model = model
        self._model_dim = model.point_dim()
        self._result = model._result

        self._dim = self._model_dim + len(self._result["paths"])


    def point_dim(self):
        return self._dim

    def crucialness_at_point(self, p):
        return self._cal_crucialness(p[0:self._model_dim], p[self._model_dim:])

    def is_feasible_point(self, p):

        if not self._model.is_feasible_point(p[0:self._model_dim]):
            return False
        
        for val in p[self._model_dim:]:
            if val < 0.0 or val > 1.0:
                return False

        return True
    
    def projected_point(self, p):
        return  np.clip(p, 0.0, 1.0)

    def random_point(self, center, radius):
        p = np.array([-1] * self._dim)

        while not self.is_feasible_point(p):
            p = np.concatenate( [self._model.random_point(center[0:self._model_dim], radius),
                                 (np.random.rand(self._dim - self._model_dim) - 0.5) * radius +
                                 center[self._model_dim:]])

        return np.array(p)
        
    def print_point(self, p):
        self._model.print_point(p[0:self._model_dim])
        print("path_weights: " + ".".join([str(i) for i in p[self._model_dim:]]))

    def _cal_crucialness(self, pars, path_weights):
        """
        We calculate the affect of dropping each residue in this function.

        For each residue, for each path, we can calculate the probability 
        of the path failure if the residue get dropped via the equation:

        P(p fail when r dropped) = 
          1 - \prod_{i \in p}(1-w_r^i)

          where w_r^i means the probability of the interface fail when r dropped.

          w_r^i is determined by the model

        And the affect of dropping the residue to the whole assembly can be calculate by
          \sum_{p \in paths} w_p \times P(p fail when r dropped)

          where w_p is the weight of each path

        Args:
        - model:
        - pars: parameters for model
        - path_weight:
        - interface_weights:

        """

        model = self._model
        
        residue_crucialness = Counter()
        for pidx, path in enumerate(self._result["paths"]):
            path_weight = path_weights[pidx]
            # print(path_weight)
            for residue in model._residue_set:
                p = 1.0
                for interface in path:
                    interface_weight = 1.0;
                    resi_crucialness = model.residue_crucialness_in_interface(residue, interface, path, 
                                                                  pars)
                    p = p * (1 - interface_weight * resi_crucialness)

                #print(pidx, residue, 1-p)
                residue_crucialness[str(residue)] += path_weight*(1 - p)
        return sorted(residue_crucialness.items(), key=lambda x:x[1], reverse=True)
    
class PathCrucialModelWithInterfaceWeight(object):
    """ Determine the 
    """

    def __init__(self, model):
        "docstring"
        self._model = model
        self._model_dim = model.point_dim()

        self._result = model._result

        self._dim_path = len(self._result["paths"])
        self._dim_interface = len(self._result["interfaces"])

        self._dim = self._model_dim + self._dim_path + self._dim_interface

        self._interface_idx = {interface:idx for idx, interface in enumerate(self._result["interfaces"])}
        
    def point_dim(self):
        return self._dim

    def crucialness_at_point(self, p):
        return self._cal_crucialness(p[0:self._model_dim],
                                     p[self._model_dim:self._model_dim + self._dim_path],
                                     p[self._model_dim + self._dim_path:])

    def is_feasible_point(self, p):

        if not self._model.is_feasible_point(p[0:self._model_dim]):
            return False
        
        for val in p[self._model_dim:]:
            if val < 0.0 or val > 1.0:
                return False

        return True
    
    def projected_point(self, p):
        # FIXME: this function should call the clip function of model
        return  np.clip(p, 0.0, 1.0)

    def random_point(self, center, radius):
        p = np.array([-1] * self._dim)

        while not self.is_feasible_point(p):
            p = np.concatenate( [self._model.random_point(center[0:self._model_dim], radius),
                                 (np.random.rand(self._dim - self._model_dim) - 0.5) * radius +
                                 center[self._model_dim:]])

        return np.array(p)
        
    def print_point(self, p):
        self._model.print_point(p[0:self._model_dim])
        print("path_weights: " + ",".join([str(i) for i in p[self._model_dim:self._model_dim + self._dim_path]]))
        print("interface_weights: " + ",".join([str(i) for i in p[self._model_dim + self._dim_path:]]))

    def _cal_crucialness(self, pars, path_weights, interface_weights):
        """
        We calculate the affect of dropping each residue in this function.

        For each residue, for each path, we can calculate the probability 
        of the path failure if the residue get dropped via the equation:

        P(p fail when r dropped) = 
          1 - \prod_{i \in p}(1-w_r^i * w_i)

          where w_r^i means the probability of the interface fail when r dropped.
          and w_i is the weight of each interface (1 for most important interface
          and 0 for least important interface

          w_r^i is determined by the model

        And the affect of dropping the residue to the whole assembly can be calculate by
          \sum_{p \in paths} w_p \times P(p fail when r dropped)

          where w_p is the weight of each path

        Args:
        - model:
        - pars: parameters for model
        - path_weight:
        - interface_weights:

        Return:
        [(resitue, crucialness)] sorted

        """

        model = self._model
        
        residue_crucialness = Counter()
        for pidx, path in enumerate(self._result["paths"]):
            path_weight = path_weights[pidx]
            for residue in model._residue_set:
                p = 1.0
                # print("--")
                for interface in path:
                    # print("ii {} {}".format(interface, self._interface_idx[interface]))
                    interface_weight = interface_weights[self._interface_idx[interface]];
                    resi_crucialness = model.residue_crucialness_in_interface(residue, interface, path, 
                                                                  pars)
                    p = p * (1 - interface_weight * resi_crucialness)

                residue_crucialness[str(residue)] += path_weight*(1 - p)
        return sorted(residue_crucialness.items(), key=lambda x:x[1], reverse=True)

        
def rank_of_subset(rank, subset):
    return [residue for residue, val in rank if residue in subset]

def sigmoid(x):
    """
    continues sigmoid function.

    f(x) = 1 when x > 0
    f(x) = 0 when x < 0
    
    """
    return 1/(1+np.exp(-x))
    
def cost_func_cont_with_training_set(training_set):
    def cost_func_cont(crucialness):
        """
        Given the training set and affect function of dropping every residue, 
        calculate the cost function.

        The training set is a set of residues pairs sorted in descending order 
        of crucialness.
        
        the cost function is:
        C = \sum_{i,j \in training set and ord(i) < ord(j) in trainning set} 
                   sigmoid(affect(i) - affect(j))

        if we make a ranking of residue from the affect function, and compare it to 
        the training set, the value of cost function is roughly the number of 
        reverse ordered pair of residues 
        in the ranking. If the ranking of residue by the affect function is 
        exactly the same as the training set, the cost function is close to 0
        """
        prob_dict = {str(residue):p for residue,p in crucialness}

        val_range = (max(prob_dict.values()) - min(prob_dict.values()))/len(prob_dict)
        if val_range == 0.0:
            val_range = 1.0
            
        res = 0
        
        for r1,r2 in training_set:
            pair_cost = sigmoid((prob_dict[r2]-prob_dict[r1])/(val_range*5))
            res += math.log(1+pair_cost)
            #res += pair_cost
            #logging.debug("r1,r2 = {},{},{}".format(r1,r2, pair_cost))
            #logging.debug("prob_dict[r1] = {}".format(prob_dict[r1]))
            #logging.debug("prob_dict[r2] = {}".format(prob_dict[r2]))

        return res
    return cost_func_cont

def cost_func_non_cont_with_training_set(training_set):
    def cost_func_non_cont(crucialness):
        """
        Simply check the number of reversed ordered pair
        """
        total_count_revered = 0;
        
        prob_dict = {str(residue):p for residue,p in crucialness}
        
        for r1,r2 in training_set:
            if prob_dict[r2] > prob_dict[r1]:
                total_count_revered += 1
            # pair_cost = sigmoid((prob_dict[r2]-prob_dict[r1])/(val_range*5))
            # res += math.log(pair_cost)
            #logging.debug("r1,r2 = {},{},{}".format(r1,r2, pair_cost))
            #logging.debug("prob_dict[r1] = {}".format(prob_dict[r1]))
            #logging.debug("prob_dict[r2] = {}".format(prob_dict[r2]))

        return total_count_revered
    return cost_func_non_cont
    
def cost_func_non_cont(rank,ranking_result):
    """ non-continuos version of the cost function for optimizing the ranking

    Args:
    - rank:

    Return:
    - reverse_count/total_count
    """
    
    myrank = []
    total_count = 0
    reverse_count = 0

    for residue,p in rank:
        if str(residue) in ranking_result:
            myrank.append(ranking_result.index(str(residue)))
    for i,j in itertools.combinations(range(len(myrank)), 2):
        total_count += 1
        if myrank[i] > myrank[j]:
            reverse_count += 1
        
    return reverse_count/total_count


def partial_at_direction(p, func, direction, step):
    """ Calcualte the partial direvitive a function at point p

    Args:
    - p: point
    - func: function
    - direction: component
    - step: step size
    """
    
    pprev = np.array(p)
    pprev[direction] -= step
    pnext = np.array(p)
    pnext[direction] += step
    #print(direction, step,  pprev, pnext, func(pnext))
    return (func(pnext) - func(pprev))/(2*step)

def gradient(p, func, step):
    """ Calcualte the numerical gradient of a function at point p

    Args:
    - p: point
    - func: function
    - step: step size
    """
    
    dim = len(p)
    grad = np.zeros(dim)
    for i in range(dim):
        grad[i] = partial_at_direction(p,func, i, step)

    return grad

def norm(p):
    return np.sqrt(np.dot(p,p))




def find_optimal_point(training_set, curr_model):
    """ Gradient descent on a training set
    """
    
    iters = 0
    mincost = 100
    minp = None

    cost_func = cost_func_cont_with_training_set(training_set)
    
   # number of initial point 
    while iters < 20:
        iters += 1

        # get a random initial point
        center = [0.5] * curr_model.point_dim()
        p = curr_model.random_point(center, 1.0)
        
        eps = 0.01
        iter_step_size = 0.01
        last_feasible_point = p
        steps = 0
        cost = 100;

        while steps < 200:
            steps += 1
            last_feasible_point = p


            logging.debug("point = {}, cost = {}, step = {}".format(
                last_feasible_point, cost,steps))

            grad = gradient(p, lambda x: cost_func(curr_model.crucialness_at_point(x)), 0.001)
            logging.debug("gradient = {}".format(grad))
            
            # alpha = 0.5
            alpha = 0.5 / steps
            # print(math.log(math.log(steps+2))/steps)
            q = 1.0 * math.sqrt(math.log(math.log(steps+2))/steps)
            
            #logging.debug("point before projection = {}".format(p - grad * alpha))
            # newp = curr_model.projected_point(p - grad * alpha + q * np.random.normal(0,1,curr_model.point_dim()))
            newp = curr_model.projected_point(p - grad * alpha)
            #logging.debug("point after projection = {}".format(newp))

            while cost_func(curr_model.crucialness_at_point(newp)) > cost:
                alpha /= 2.0
                newp = curr_model.projected_point(p - grad * alpha)

            if norm(newp - p) < eps:
                break

            old_cost = cost
            # cost = curr_model.cost_func_at_point(newp, cost_func)
            cost = cost_func(curr_model.crucialness_at_point(newp))
        
            
            # logging.info("cost = {}, step = {}, diff = {}".format(cost,steps, norm(newp-p)))

            p = newp
            
            alpha_factor = 2.0

            # if cost > 7:
            #     alpha = 0.1 * alpha_factor
            # elif cost > 6.2:
            #     alpha = 0.05 * alpha_factor
            # elif cost > 6.5:
            #     alpha = 0.005 * alpha_factor
            # else:
            #     alpha = 0.001 * alpha_factor

        logging.info("point = {}, cost = {},  steps = {}".format(last_feasible_point, cost, steps))
        #crucialness = cal_crucialness(curr_model, last_feasible_point[0:3], last_feasible_point[3:])
        
        if cost < mincost:
            mincost = cost
            minp = last_feasible_point
    return minp, mincost

def processCommandLineArgs():
    
    """ handle arguments """
    parser = argparse.ArgumentParser()
    parser.add_argument("data_directory", help="data directory")
    parser.add_argument("result_file", help="RESULT.json")
    parser.add_argument("-d","--debug", action="store_true", help="show debug infomation")
    parser.add_argument("-o","--output", help="output filename")

    return parser.parse_args()
    
def main():
    args = processCommandLineArgs()
    
    print_level = logging.INFO
    if args.debug:
        print_level = logging.DEBUG

    output_filename = "learning_result.pkl"
    if args.output:
        output_filename = args.output
        
    logging.basicConfig(format='[%(levelname)s][%(filename)s#%(lineno)s]:%(message)s', level=print_level)

    filename = args.result_file
    workspace = args.data_directory

    logging.info("reading {}".format(filename))

    with open(filename, 'r') as f:
        result = json.load(f)

    logging.info("getting unified ratio stat...")
    #ratio_stat = get_unified_ratio_stat(workspace)
    ratio_stat = get_ratio_stats(workspace, include_neighbor=False)

    print(ratio_stat)

    # get all pair of residues from different ranking group
    all_pairs = [ (r1,r2) for r1,r2 in itertools.permutations(result["ranking_result"], 2) \
                  if result["ranking_result"][r1] < result["ranking_result"][r2] ]


    #curr_model = PathCrucialModel(LinearModel(result, ratio_stat, result["paths"]))
    curr_model = PathCrucialModelWithInterfaceWeight(LinearModel(result, ratio_stat, result["paths"]))

    global_optimal_point = None
    global_optimal_cost = 0.0
    num_training_set = 10
    training_set_size = int(len(all_pairs) * 0.5)

    # trainging on all different training set
    for idx in range(num_training_set):

        # generating the training set by random;y sample the residue pairs
        training_set = random.sample(all_pairs, training_set_size)
        logging.info("training set #{}/{}".format(idx+1,num_training_set))
        logging.info("training set: {}".format(training_set))

        cost_func_total = cost_func_cont_with_training_set(all_pairs)

        logging.info("start learning with training set #{}".format(idx))

        minp,mincost = find_optimal_point(training_set, curr_model)

        print("optimal cost for current training set: {}", mincost)
        print("optimal point:")
        curr_model.print_point(minp)


        total_crucialness = curr_model.crucialness_at_point(minp)
        print(rank_of_subset(total_crucialness, result["ranking_result"].keys()))

        global_cost = cost_func_total(curr_model.crucialness_at_point(minp))
        if global_optimal_point is None or global_cost < global_optimal_cost:
            global_optimal_point = minp
            global_optimal_cost = global_cost

        # save result after each training set in case bad things happen
        total_crucialness = curr_model.crucialness_at_point(global_optimal_point)
        with open(output_filename, "wb") as f:
            pickle.dump(total_crucialness, f)

    print("global optimal cost = {}".format(global_optimal_cost))
    curr_model.print_point(global_optimal_point)

    total_crucialness = curr_model.crucialness_at_point(global_optimal_point)
    print(rank_of_subset(total_crucialness, result["ranking_result"].keys()))

    with open(output_filename, "wb") as f:
        pickle.dump(total_crucialness, f)

    print(total_crucialness)

def test():
    args = processCommandLineArgs()
    
    print_level = logging.INFO
    if args.debug:
        print_level = logging.DEBUG
        
    logging.basicConfig(format='[%(levelname)s][%(filename)s#%(lineno)s]:%(message)s', level=print_level)

    filename = args.result_file
    workspace = args.data_directory

    logging.info("reading {}".format(filename))

    with open(filename, 'r') as f:
        result = json.load(f)

    logging.info("getting unified ratio stat...")
    ratio_stat = get_ratio_stats(workspace)

    # get all pair of residues from different ranking group
    all_pairs = [ (r1,r2) for r1,r2 in itertools.permutations(result["ranking_result"], 2) \
                  if result["ranking_result"][r1] < result["ranking_result"][r2] ]


    #curr_model = PathCrucialModel(LinearModel(result, ratio_stat, result["paths"]))
    curr_model = PathCrucialModelWithInterfaceWeight(LinearModel(result, ratio_stat, result["paths"]))

    for k, v in curr_model._model._cached_stat.items():
        if k[1] == 'paav':
            print(k[2])

    # print(curr_model._model._cached_stat)
    

if __name__ == '__main__':
    # test()
    main()
