import numpy as np
import sys
import os
import re

from collections import Counter
import logging

from read_ratio_stat import read_ratio_stat
from read_helix_config import read_helix_config

def extract_residue(ele):
    m = re.match(r'(\d+)([^-]+)-(\d+)([^-]+)', ele)
    if m is None:
        return None
    else:
        return m.group(1),m.group(3)


def convert_ratio_stat_residue(ratio_stat):
    multi_host_stat = set()
    single_host_stat = set()
    
    for ele in ratio_stat:
        if extract_residue(ele) is None:
            multi_host_stat.add(ele)
        else:
            r1,r2 = extract_residue(ele)
            single_host_stat.add(r1)
            single_host_stat.add(r2)
    res = {}
    for ele, v in ratio_stat.items():
        if extract_residue(ele) is None:
            res[ele] = v
        else:
            r1,r2 = extract_residue(ele)
            if r1 not in multi_host_stat:
                res[r1] = v

            if r2 not in multi_host_stat:
                res[r2] = v
    return res

def get_ratio_stats(rootdir, include_neighbor=True):
    """ read the ratio stat data for different neighbour. 
    rootdir is the folder of every test case. The configuration file are in the
    folder {interface}_{neighbour}_hint, all the result ratio are in the ratio_stat
    folder, and the filename are ratio_stat_{interface}_{neighbour}.csv

    Return: 
        { interface : [(neighbour, {residue: (total_s, true_s, ratio_s)} )] }
    """
    test_cases = {}
    for x in os.walk(rootdir):
        path = x[0]

        basename = os.path.basename(path)

        # break the folder name into interface and neighbour
        m = re.match(r'([^_]+)_([^_]+)_hint', basename)
        if m is None:
            continue

        print(basename)
        
        print(m.groups())
        interface = m.group(1)
        neighbour = m.group(2)

        print(interface, neighbour)

        if neighbour != 'init' and not include_neighbor:
            continue

        ratio_file = os.path.join(rootdir, 'ratio_stat', 
                                  "ratio_stat_{}_{}.csv".format(interface, neighbour))

        # if not ratio_stat file, skip it
        if not os.path.exists(ratio_file):
            logging.debug("file {} not exist, skipping {}".format(ratio_file, basename)) 
            continue

        # read the configuration file to get dist and pair file
        config_file = os.path.join(path, "HelixPacking.cfg")

        config = read_helix_config(config_file)

        workspace = config["workspace"]
        pairs_file = os.path.join(workspace,'pairs.txt')
        dist_file = config["dist"]

        print(workspace, ratio_file, pairs_file, dist_file)

        ratio_stat = read_ratio_stat(dist_file, pairs_file, ratio_file, use_residue=True)
        ratio_stat_residue = convert_ratio_stat_residue(ratio_stat)

        print(ratio_stat_residue)

        if not interface in test_cases:
            test_cases[interface] = [(neighbour,ratio_stat_residue)]
        else:
            test_cases[interface].append((neighbour, ratio_stat_residue))
            
    return test_cases

def get_unified_ratio_stat(rootdir):
    """ read the ratio stat data for different neighbour and average them.
    rootdir is the folder of every test case. The configuration file are in the
    folder {interface}_{neighbour}_hint, all the result ratio are in the ratio_stat
    folder, and the filename are ratio_stat_{interface}_{neighbour}.csv

    Return: 
        { interface : (total_s, true_s, ratio_s) }
    """

    test_cases = get_ratio_stats(rootdir)
    
    res = {}
    for interface in test_cases:
        logging.info("unifying interface {}".format(interface))
        total_s = Counter()
        ratio_s = Counter()
        true_s = Counter()

        for neighbour, ratio_stat in test_cases[interface]:
            logging.info("nei = {}".format(neighbour))
            for ele, v in sorted(ratio_stat.items()):
                total, true, ratio = v
                total_s[ele] += total
                ratio_s[ele] += ratio
                true_s[ele] += true
                logging.debug("{},{},{},{}".format(ele,total, true, ratio))

        num_nei = len(test_cases[interface])

        logging.debug("overall")
        for ele in total_s:
            total_s[ele] /= num_nei
            ratio_s[ele] /= num_nei
            true_s[ele] /= num_nei
            logging.debug("{},{},{},{}".format(ele,total_s[ele], true_s[ele], ratio_s[ele]))

        res[interface] = {ele:np.array([total_s[ele], true_s[ele], ratio_s[ele]])
                          for ele in total_s.keys()}

    return res

if __name__ == '__main__':
    #logging.basicConfig(level=logging.INFO)
    logging.basicConfig(level=logging.DEBUG)
    # print(get_unified_ratio_stat("../aav"))
    print(get_ratio_stats("../aav", include_neighbor=False))
    
    
