#!/usr/bin/python

import sys
f = open(sys.argv[1],'rb')
lin_num = 0
for line in f:
    lin_num = lin_num + 1
    row = line.split()
    
    if len(row) < 1:
        continue

    if row[0] == 'ATOM':
        if len(row) == 12: 
            print(' '.join([row[0],row[1],row[5]+row[2],row[6],row[7],row[8],"1.00"]))
        elif len(row) == 11:
            print(' '.join([row[0],row[1],row[4]+row[2],str(-float(row[5])),str(-float(row[6])),row[7],'1.00']))
            #print(' '.join([row[0],row[1],row[5]+row[2],row[6],row[7],row[8],row[9]]))
        elif len(row) == 10: 
            print(' '.join([row[0],row[1],row[4]+row[2],row[5],row[6],row[7],"1.00"]))
        else:
            sys.stderr.write('error line %d,%d\n' % (lin_num,len(row)))
    elif row[0] == 'END':
        break

f.close()
