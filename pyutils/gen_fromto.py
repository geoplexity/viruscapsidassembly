#!/usr/bin/python
import sys
import math
import readline
import random

from read_pdb import *
from read_dist import *
from trans import *

def dist(p1,p2):
    return math.sqrt(sum(map(lambda x:x*x, map(lambda x: x[0]-x[1],zip(p1,p2)))))
    #l = 0
    #for i in range(0,3):
    #    l = l+(p1[i]-p2[i])*(p1[i]-p2[i])
    #return math.sqrt(l)

def get_random_name(p1,p2):
    count = 0
    while count < 100:
        count += 1
        name = p1.keys()[rand.randint(0,len(p1)-1)]
        if name in p2:
            return name
    print("can't find common name") 
    return None        

def get_many_random_name(p1,p2,num_name):
    names = set()
    while(len(names)<num_name):
        names.add(get_random_name(p1,p2))
    return tuple(names)
        
    

#filename1 = raw_input('please enter the filename of 1st pdb:');
#filename2 = raw_input('please enter the filename of 2nd pdb:');

#filename1 = "newaav.pdb"
#filename2 = "new5f3.txt"

filename1 = sys.argv[1]
filename2 = sys.argv[2]
dist_file = sys.argv[3]

pdb1 = read_pdb(filename1)
pdb2 = read_pdb(filename2)

dists = read_dist(dist_file)

rand = random.Random()

res = np.zeros(12)
count = 0
for i in xrange(40):
    a1,a2,a3 = get_many_random_name(pdb1,pdb2,3)

    mat = np.vstack([pdb1[a1],pdb1[a2],pdb1[a3]])
    if np.linalg.cond(mat) > 1000:
        continue;
    mat = getTransMatrix([pdb1[a1],pdb1[a2],pdb1[a3]],[pdb2[a1],pdb2[a2],pdb2[a3]])
    res += mat.ravel()
    count += 1

res = res/float(count)
print('res',np.reshape(res,(3,-1)))

count = 1000

while count > 0:
    count -= 1

    a1,a2,a3 = get_many_random_name(pdb1,pdb2,3)

    mat = np.vstack([pdb1[a1],pdb1[a2],pdb1[a3]])
    if np.linalg.cond(mat) > 1000:
        #print("cond too high:%f"%np.linalg.cond(mat))
        continue;
    mat = getTransMatrix([pdb1[a1],pdb1[a2],pdb1[a3]],[pdb2[a1],pdb2[a2],pdb2[a3]])
    if dist(mat.ravel(),res) > 0.01:
        #print("error too high: %f"%dist(mat.ravel(),res))
        continue

    print(' '.join(map(str,pdb1[a1])))
    print(' '.join(map(str,pdb2[a1])))
    print(' '.join(map(str,pdb1[a2])))
    print(' '.join(map(str,pdb2[a2])))
    print(' '.join(map(str,pdb1[a3])))
    print(' '.join(map(str,pdb2[a3])))

    break

if count <= 0:
    print("can't find transform matrix after 1000 try")
