import sys
import numpy as np

from read_pdb import read_pdb

pdb1 = read_pdb(sys.argv[1])
pdb2 = read_pdb(sys.argv[2])

threshold = 3.4

interactions = []

for a1 in pdb1:
    pos1 = pdb1[a1]
    for a2 in pdb2:
        pos2 = pdb2[a2]
        dis = np.linalg.norm(pos1-pos2)
        if dis < threshold:
            interactions.append((a1,a2,dis))
interactions.sort(key = lambda x: x[2])

for a1,a2,dis in interactions:
    print('{0} {1} {2}'.format(a1,a2,dis))
        
