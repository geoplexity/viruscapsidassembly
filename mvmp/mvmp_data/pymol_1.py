from pymol import stored

import re

def breakname(name):
    pattern = re.compile(r'(^\d+)(.*$)') 
    return pattern.split(name)[1:3]

def readdistlist(filename):
    res = []
    f = open(filename,'rb')
    for line in f:
        segs = line.split();
        atom1 = breakname(segs[0])
        atom2 = breakname(segs[1])
        res.append((atom1,atom2))
    f.close()
    return res

def connect_dists(mol1,mol2,filename):
    dist_list = readdistlist(filename)
    for dist in dist_list:
        cmd.distance('%s%s-%s%s'%tuple(dist[0]+dist[1]), 
                    '/%s///%s/%s'%(mol1,dist[0][0],dist[0][1]), 
                    '/%s///%s/%s'%(mol2,dist[1][0],dist[1][1]))
    

cmd.extend("connect_dists",connect_dists)
