#!/usr/bin/python

import sys
filename = sys.argv[1]

f = open(filename, 'rb')
for line in f:
    parts = line.split()
    if len(parts) < 7:
        continue
    print('%s%s %s%s %s'%(parts[1],parts[2],parts[4],parts[5],parts[6]))
f.close()
