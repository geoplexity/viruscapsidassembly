SER  43   OG      ASP  263  OD2         2.96993

  TYR  47   OH      GLU  251  CG          3.44679

  TYR  47   CD2     ILE  256  CD1         3.49215

  TYR  47   CB      LEU  258  CD1         3.45737

  ASN  149  OD1     ARG  260  NE        3.03425

  VAL  151  CG1     THR  173  OG1       3.47023

  LYS  153  NZ      ASP  171  OD2         2.63676

  LYS  153  NZ      ASN  504  OD1        3.16946

  TYR  168  OH      THR  506  CB         3.48263

  TYR  168  OH      ASP  507  OD2        3.19259

  ASN  170  ND2     ASP  171  OD1        2.84189

  ASN  170  OD1     THR  173  OG1       3.04124

  THR  261  OG1     ARG  260  NH1       2.64466

