#!/usr/bin/python
import sys
import math
import readline

def dist(p1,p2):
    l = 0
    for i in range(0,3):
        l = l+(p1[i]-p2[i])*(p1[i]-p2[i])
    return math.sqrt(l)


#filename1 = raw_input('please enter the filename of 1st pdb:');
#filename2 = raw_input('please enter the filename of 2nd pdb:');

#filename1 = "newaav.pdb"
#filename2 = "new5f3.txt"

filename1 = sys.argv[1]
filename2 = sys.argv[2]

f1 = open(filename1,'rb')
f2 = open(filename2,'rb')

pdb1 = {}
pdb2 = {}

for line in f1:
    row = line.split()
    pdb1[row[2]] = (float(row[3]),float(row[4]),float(row[5]))


for line in f2:
    row = line.split()
    pdb2[row[2]] = (float(row[3]),float(row[4]),float(row[5]))

f1.close()
f2.close()

#dist_filename = raw_input('please enter the file name of distance:')
#dist_filename = "dist5.txt"
dist_filename = sys.argv[3]

dists = []

fd = open(dist_filename,'r')
for line in fd:
    row = line.split()
    if len(row) < 3:
	continue

    dis1 = dist(pdb1[row[0]],pdb2[row[1]])
    dis2 = dist(pdb2[row[0]],pdb1[row[1]])

    if abs(dis1-float(row[2])) < abs(dis2-float(row[2])):
        dists.append([row[0],row[1],float(row[2]),dis1])
    else:
        print('flip')
        dists.append([row[1],row[0],float(row[2]),dis2])
    #print row[0],row[1],row[2],dist(pdb1[row[0]],pdb2[row[1]])
    print dist(pdb1[row[0]],pdb1[row[1]]),dist(pdb2[row[0]],pdb2[row[1]])
    print pdb1[row[0]]
    print pdb2[row[0]]

dists.sort(key=lambda x:x[0])

for d in dists:
	print '\t'.join(map(str,d))

li = {}


while True:
    aname1 = raw_input('please enter the 1st atom:')
    aname2 = raw_input('please enter the 2nd atom:')
    print pdb1[aname1]
    print pdb2[aname2]
    print dist(pdb1[aname1],pdb2[aname2])
