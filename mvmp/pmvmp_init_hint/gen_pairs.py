#!/use/bin/python

f = open('ndist5.txt','rb')

h1 = []
h2 = []
pairs = []

for line in f:
    parts = line.split()
    if len(parts) < 3:
        continue
    pairs.append((parts[0],parts[1]))

    if not (parts[0] in h1):
        h1.append(parts[0])
    if not (parts[1] in h2):
        h2.append(parts[1])

for pair in pairs:
    print('%d,%d'%(h1.index(pair[0]),h2.index(pair[1])))

f.close()
