import sys

if len(sys.argv) < 2:
    sys.exit()

filename = sys.argv[1]

f = open(filename)
pairs = []

for line in f:
    pairs.append(line.split())

f.close()
    
pairs.sort(key=lambda x:x[0])

for dist in pairs:
    print(' '.join(dist))
